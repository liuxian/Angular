'use strict';


angular.module('app', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'ngStorage',
    'ui.select',
    'toaster',
    'ui.router',
    'ui.bootstrap',
    'ui.load',
    'dbUtils',
    'ui.components.form.grid',
    'ui.jq',
    'ui.validate',
    'oc.lazyLoad',  
    'pascalprecht.translate'
]);